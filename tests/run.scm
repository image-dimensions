(import scheme)
(cond-expand
 (chicken-4
  (use image-dimensions test))
 (chicken-5
  (import image-dimensions)
  (import test)))

(define (image-info-for path)
  (call-with-input-file path image-info #:binary))

(define (image-dimensions-for path)
  (call-with-input-file path image-dimensions #:binary))

(test-group "GIF"
  (test '(gif 10 20 0) (image-info-for "10x20.gif"))
  (test '(10 20) (image-dimensions-for "10x20.gif")))

(test-group "PNG"
  (test '(png 10 20 0) (image-info-for "10x20.png"))
  (test '(10 20) (image-dimensions-for "10x20.png")))

(test-group "TIFF"
  (test '(tiff 10 20 0) (image-info-for "10x20_lsb.tiff"))
  (test '(10 20) (image-dimensions-for "10x20_lsb.tiff"))
  (test '(tiff 10 20 0) (image-info-for "10x20_msb.tiff"))
  (test '(10 20) (image-dimensions-for "10x20_msb.tiff")))

(test-group "JPEG"
  (test '(jpeg 10 20 0) (image-info-for "10x20.jpg"))
  (test '(10 20) (image-dimensions-for "10x20.jpg"))
  (test '(jpeg 10 20 0) (image-info-for "10x20_1.jpg"))
  (test '(10 20) (image-dimensions-for "10x20_1.jpg"))
  (test '(jpeg 10 20 0) (image-info-for "10x20_2.jpg"))
  (test '(10 20) (image-dimensions-for "10x20_2.jpg"))
  (test '(jpeg 10 20 180) (image-info-for "10x20_3.jpg"))
  (test '(10 20) (image-dimensions-for "10x20_3.jpg"))
  (test '(jpeg 10 20 180) (image-info-for "10x20_4.jpg"))
  (test '(10 20) (image-dimensions-for "10x20_4.jpg"))
  (test '(jpeg 10 20 90) (image-info-for "20x10_5.jpg"))
  (test '(10 20) (image-dimensions-for "20x10_5.jpg"))
  (test '(jpeg 10 20 90) (image-info-for "20x10_6.jpg"))
  (test '(10 20) (image-dimensions-for "20x10_6.jpg"))
  (test '(jpeg 10 20 270) (image-info-for "20x10_7.jpg"))
  (test '(10 20) (image-dimensions-for "20x10_7.jpg"))
  (test '(jpeg 10 20 270) (image-info-for "20x10_8.jpg"))
  (test '(10 20) (image-dimensions-for "20x10_8.jpg")))

(test-exit)
