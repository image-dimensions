## About

Scheme port of the [dimensions gem] for reading the width, height and
rotation of GIF, PNG, TIFF and JPEG images.

## Docs

See [its wiki page].

[dimensions gem]: https://github.com/sstephenson/dimensions
[its wiki page]: http://wiki.call-cc.org/eggref/5/image-dimensions
